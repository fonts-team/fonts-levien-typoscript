fonts-levien-typoscript (000.001-5) unstable; urgency=medium

  * debian/control
    - Update Maintainer address
    - Remove Christian Perrier from Uploaders (Closes: #927626)
    - Use dh13
    - Set Standards-Version: 4.5.0
    - Use https for Homepage:
    - Update Vcs-* to point salsa.debian.org
    - Add Rules-Requires-Root: no
    - Remove obsolete dependencies to ttf-levien-typoscript
  * debian/rules
    - Use default compression
  * debian/{dirs,install}
    - Move install location to s/truetype/opentype/
  * Add debian/salsa-ci.yml

 -- Hideki Yamane <henrich@debian.org>  Sun, 09 Aug 2020 20:53:25 +0900

fonts-levien-typoscript (000.001-4) unstable; urgency=low

  * Update Standards to 3.9.5 (checked)
  * Bump debhelper compatibility to 9
  * Drop transitional package
  * Add Multi-Arch: foreign field
  * Use Breaks instead of Conflicts. Drop Provides as it is no
    longer needed (installations should have transitioned since wheezy
    and the package has anyway no reverse dependency.
  * Use correct Replaces and Breaks on ttf-levien-typoscript and not
    ttf-levien-museum
  * Use xz extreme compression for deb packages
  * Use git for packaging: adapt Vcs-* fields
  * Complete copyright statement in debian/copyright

 -- Christian Perrier <bubulle@debian.org>  Fri, 03 Jan 2014 13:53:20 +0100

fonts-levien-typoscript (000.001-3) unstable; urgency=low

  * Team upload
  * Rename source package to "fonts-levien-typoscript" to fit the Font
    Packages Naming Policy.
  * Bump Standards to 3.9.2 (checked)
  * Change fonts install directory from
    usr/share/fonts/truetype/ttf-levien-typoscript-* to
    usr/share/fonts/truetype/levien
  * Bump debhelper compatibility to 8

 -- Christian Perrier <bubulle@debian.org>  Sat, 22 Oct 2011 14:46:34 +0200

ttf-levien-typoscript (000.001-2) unstable; urgency=low

  * Use "fontforge-nox | fontforge" in build dependencies
  * Bump Standards to 3.9.1

 -- Christian Perrier <bubulle@debian.org>  Sat, 12 Feb 2011 14:18:39 +0100

ttf-levien-typoscript (000.001-1) unstable; urgency=low

  [ Nicolas Spalinger ]
  * Initial test release of the work in progress on the Typo Script font
    released under OFL (Closes: #561022)

 -- Christian Perrier <bubulle@debian.org>  Wed, 16 Dec 2009 22:58:10 +0100
